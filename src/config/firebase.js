import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyAc7yRbOcJ4TeDFkZYzp12I5OCbNoLQPEg",
  authDomain: "chousei-firebase-b937d.firebaseapp.com",
  databaseURL: "https://chousei-firebase-b937d.firebaseio.com",
  projectId: "chousei-firebase-b937d",
  storageBucket: "",
  messagingSenderId: "813098403170",
  appId: "1:813098403170:web:61093d88e5fe96bf49d83b"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
